Bobae Choi (301265433) || Steven Lee (301250558)

Kernel version = v3.19.1

1.) mkdir cs300 in folder linux-stable
1a.) transfer our cs300 contents in the newly made cs300 folder

2.) add

ifeq ($(KBUILD_EXTMOD),)
core-y		+= kernel/ mm/ fs/ ipc/ security/ crypto/ block/ cs300/

to the main Makefile in linux-stable

3.) add 

330 common cs300_test sys_cs300_test
331 common array_stats sys_array_stats
332 common process_ancestors sys_process_ancestors

to syscall_64.tbl if not already set in the arch/x86/syscalls folder
(the syscall_64.tbl folder should already be included in this tarball)

4.) add tests folder one level above linux-stable
4a.) go inside tests, then make using the makefile

5.) rebuild the kernel with make -j4

6.) run with 

qemu-system-x86_64 -m 64M -hda ../debian_squeeze_amd64_standard.qcow2 -append "root=/dev/sda1 console=tty0 console=ttyS0,115200n8" -kernel arch/x86_64/boot/bzImage -nographic -net nic,vlan=1 -net user,vlan=1 -redir tcp:2222::22

to boot qemu

7.) transfer test files in correct directory to qemu with

scp -P 2222 cs300_testapp root@localhost:~ OR
scp -P 2222 array_test root@localhost:~ OR
scp -P 2222 test_ancestors root@localhost:~

8.) run the files in qemu with

cd/root THEN

./cs300_testapp OR
./array_test OR
./test_ancestors