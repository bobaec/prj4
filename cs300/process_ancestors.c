#include <linux/kernel.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <linux/cred.h>

#include "process_ancestors.h"

asmlinkage long sys_process_ancestors(struct process_info info_array[], long size, long *num_filled) {
	int count = 0;
	int childCount = 0;
	int siblingCount = 0;

	struct task_struct *cur_task = current;

	if (size <= 0) {
		return -EINVAL;
	}

	if (cur_task->parent == cur_task) {
		return 0;
	}

	while (cur_task->parent != cur_task) {

		if (copy_to_user(info_array, &cur_task, sizeof(cur_task))) {
			return -EFAULT;
		}

		struct process_info processInfo;

		struct task_struct *taskChild;
		struct list_head *listChild;

		struct task_struct *taskSibling;
		struct list_head *listSibling;

		processInfo.pid = (long) current->pid;
		printk("PID = %ld\n", processInfo.pid);

		strcpy(processInfo.name, cur_task->comm);
		printk("Name = %s\n", processInfo.name);

		processInfo.state = (long) cur_task->state;
		printk("State = %ld\n", processInfo.state);

		kuid_t uid = cur_task->cred->uid;
		processInfo.uid = (long) uid.val;
		printk("UID = %ld\n", processInfo.uid);

		processInfo.nvcsw = cur_task->nvcsw;
		printk("NVCSW = %ld\n", processInfo.nvcsw);

		processInfo.nivcsw = cur_task->nivcsw;
		printk("NIVCSW = %ld\n", processInfo.nivcsw);

		list_for_each(listChild, &cur_task->children) {
			taskChild = list_entry(listChild, struct task_struct, children);
			childCount++;
		}
		processInfo.num_children = childCount;
		printk("Children = %ld\n", processInfo.num_children);

		list_for_each(listSibling, &cur_task->sibling) {
			taskSibling = list_entry(listSibling, struct task_struct, sibling);
			siblingCount++;
		}
		processInfo.num_siblings = siblingCount;
		printk("Siblings = %ld\n", processInfo.num_siblings);

		cur_task = cur_task->parent;
		info_array[count] = processInfo;
		count++;
		printk("DONE\n");
	}

	return 0;

}