#include <linux/kernel.h>

asmlinkage long sys_cs300_test(int argument) {
	long result = 0;
	printk("Hello World!\n");
	printk("--syscall argument %d\n", argument);

	result = argument + 1;
	printk("--returning %d +  = ld\n", argument, result);
	return result;
}