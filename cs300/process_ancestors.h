#ifndef _PROCESS_ANCESTORS_H
#define _PROCESS_ANCESTORS_H

#define ANCESTOR_NAME_LEN 16

struct process_info {
	long pid;
	char name[ANCESTOR_NAME_LEN];
	long state;
	long uid;
	long nvcsw;
	long nivcsw;
	long num_children;
	long num_siblings;
};

#endif