#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#include "array_stats.h"

asmlinkage long sys_array_stats(struct array_stats *stats, long data[], long size) {
	struct array_stats *arrayStats = kmalloc(sizeof(struct array_stats), GFP_KERNEL);
	long dataStats = 0;
	int i = 0;

	if (size <= 0) {
		return -EINVAL;
	}

	if (copy_to_user(stats, &arrayStats, sizeof(arrayStats))) {
		return -EFAULT;
	}

	while (i < size) {
		if (copy_from_user(&dataStats, &data[i], sizeof(data[i]))) {
			return -EFAULT;
		}

		if (dataStats < arrayStats->min) {
			arrayStats->min = dataStats;
			printk("MIN = %ld\n", arrayStats->min);
		}
		if (dataStats > arrayStats->max) {
			arrayStats->max = dataStats;
			printk("MAX = %ld\n", arrayStats->max);
		}
		arrayStats->sum += dataStats;
		printk("SUM = %ld\n", arrayStats->sum);
		i++;
	}

	kfree(arrayStats);
	return 0;
}