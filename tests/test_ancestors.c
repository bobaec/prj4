#include <stdio.h>

#include <unistd.h>

#include <sys/syscall.h>

#define _PROCESS_ANCESTORS_ 332
#define ANCESTOR_NAME_LEN 16

struct process_info {
	long pid;
	char name[ANCESTOR_NAME_LEN];
	long state;
	long uid;
	long nvcsw;
	long nivcsw;
	long num_children;
	long num_siblings;
};
long size = 8;
struct process_info info[8];
long *num_filled;

int main(int argc, char *argv[]) {
	printf("\nDiving to kernel level\n\n");
	int result = syscall(_PROCESS_ANCESTORS_, info, size, num_filled);
	printf("\nRising to user level w/ result = %d\n\n", result);
	return 0;
}