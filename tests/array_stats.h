#ifndef _ARRAY_STATS_H
#define _ARRAY_STATS_H

struct array_stats {
	long min;
	long max;
	long sum;
};

#endif