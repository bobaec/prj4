#include <stdio.h>

#include <unistd.h>

#include <sys/syscall.h>

#define _ARRAY_STATS_ 331

struct array_stats {
	long min;
	long max;
	long sum;
};

struct array_stats stats = {500, 0, 0};

long data[5] = {1, 50, 100, 200, 400};
int size = 5;

int main(int argc, char *argv[]) {
	printf("\nDiving to kernel level\n\n");
	int result = syscall(_ARRAY_STATS_, &stats, data, size);
	printf("\nRising to user level w/ result = %d\n\n", result);

	return 0;
}